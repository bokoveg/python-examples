#!/usr/bin/python3


class InstanceSemaphore(type):
    def __init__(cls, name, bases, attrs, **kwargs):
        cls.counter = {}
        super(InstanceSemaphore).__init__(name, bases, attrs)

    def __call__(cls, *args, **kwargs):
        a = super(InstanceSemaphore).__call__(*args, **kwargs)
        if type(a).__name__ not in cls.counter:
            cls.counter[type(a).__name__] = 0

        if cls.counter[type(a).__name__] == a.__max_instance_count__:
            raise TypeError
        else:
            cls.counter[type(a).__name__] += 1
            return a


class A(metaclass=InstanceSemaphore):
    __max_instance_count__ = 2


class B(metaclass=InstanceSemaphore):
    __max_instance_count__ = 1


if __name__ == '__main__':
    a_one = A('one')
    a_two = A('two')
    b_one = B('one')

