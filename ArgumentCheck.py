#!/usr/bin/env python3
import sys
import functools


def takes(*types):
    def wraps(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            for i in range(min(len(args), len(types))):
                if not isinstance(args[i], types[i]):
                    raise TypeError
            return func(*args, **kwargs)
        return wrapper
    return wraps


if __name__ == '__main__':
    exec(sys.stdin.read())
