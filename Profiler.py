#!/usr/bin/env python3
import functools
import time


def profiler(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if wrapper.init:
            wrapper.calls = 1
            wrapper.init = False
            wrapper.start = time.clock()
            res = func(*args, **kwargs)
            wrapper.last_time_taken = time.clock() - wrapper.start
            wrapper.init = True
            return res
        else:
            wrapper.calls += 1
            return func(*args, **kwargs)
    wrapper.init = True
    wrapper.calls = 0
    wrapper.last_time_taken = 0
    return wrapper
