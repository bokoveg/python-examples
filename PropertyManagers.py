import contextlib
import traceback
import sys

@contextlib.contextmanager
def supresser(*args):
    try:
        yield {}
    except Exception as e:

        if type(e) not in args:
            raise e


@contextlib.contextmanager
def retyper(type_from, type_to):
    try:
        yield {}
    except Exception as e:
        if type(e) == type_from:
            new_e = type_to()
            new_e.__traceback__ = e.__traceback__
            new_e.args = e.args
            raise new_e


@contextlib.contextmanager
def dumper(stream):
    try:
        yield {}
    except Exception as e:
        formatted_lines = traceback.format_exc().splitlines()
        stream.write(formatted_lines[-1] + '\n')
        for line in formatted_lines[0: -1]:
            stream.write(line + '\n')
        raise e

