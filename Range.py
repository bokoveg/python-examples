#!/usr/bin/python3
class Range:

    def __init__(*args, **kwargs):
        self = args[0]
        if len(args) == 2:
            self.start = 0
            self.end = args[1]
            self.step = 1
        if len(args) == 3:
            self.start = args[1]
            self.end = args[2]
            self.step = 1
        if len(args) == 4:
            self.start = args[1]
            self.end = args[2]
            self.step = args[3]
        self.cur = self.start

    def __contains__(self, item):
        if self.start <= item < self.end and (item - self.start) % self.step == 0:
            return True
        else:
            return False

    def __getitem__(self, item):
        if item < 0:
            item += (self.end - self.start) // self.step + 1

        if item >= 0:
            if self.start + self.step * item >= self.end:
                raise KeyError
            else:
                return self.start + self.step * item

    def __iter__(self):
        return self

    def __next__(self):
        if self.cur >= self.end:
            self.cur = self.start
            raise StopIteration
        else:
            self.cur += self.step
            return self.cur - self.step

    def __len__(self):
        return (self.end - self.start + 1) // self.step

    def __repr__(self):
        if self.step == 1:
            return "range({}, {})".format(self.start, self.end)
        else:
            return "range({}, {}, {})".format(self.start, self.end, self.step)

if __name__ == '__main__':
    a = Range(1, 10, 2)
    print a
    for i in a:
        print i

