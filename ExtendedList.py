import sys


class ExtendedList(list):
    def __getattr__(self, name):
        if name == 'F' or name == 'first':
            return self[0]
        elif name == 'R' or name == 'reversed':
            return list(reversed(self))
        elif name == 'S' or name == 'size':
            return len(self)
        elif name == 'L' or name == 'last':
            return self[len(self) - 1]
        else:
            super().__getattr__(name)

    def __setattr__(self, key, value):
        if key == 'F' or key == 'first':
            self[0] = value
        elif key == 'L' or key == 'last':
            self[len(self) - 1] = value
        elif key == 'S' or key == 'size':
            if value > len(self):
                for i in range(value - len(self)):
                    self.append(None)
            else:
                for i in range(len(self) - value):
                    del self[len(self) - 1]
        else:
            super().__setattr__(key, value)


