#!/usr/bin/env python3
import functools


def cache(size):
    def wraps(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            new_hash = (hash(args), hash(tuple(kwargs.items())))

            if new_hash in wrapper.last_calc:
                return wrapper.last_calc[new_hash]
            else:
                new_calc = func(*args, **kwargs)
                wrapper.last_calc[new_hash] = new_calc
                wrapper.last_hash.append(new_hash)
                if len(wrapper.last_hash) > size:
                    del wrapper.last_calc[wrapper.last_hash[0]]
                    wrapper.last_hash.pop(0)
                return new_calc

        wrapper.last_calc = {}
        wrapper.last_hash = []
        return wrapper
    return wraps

