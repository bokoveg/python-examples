#!/usr/bin/python3
import inspect


class PropertyMaker(type):
    def __init__(cls, name, bases, attrs):
        super().__init__(name, bases, attrs)

    def __call__(cls, *args, **kwargs):
        methods = inspect.getmembers(cls)
        attr_names = []
        getters = {}
        setters = {}

        for method in methods:
            if method[0].startswith('set_'):
                varname = method[0][4:]
                if varname not in attr_names:
                    attr_names.append(varname)
                setters[varname] = method[1]

            if method[0].startswith('get_'):
                varname = method[0][4:]
                if varname not in attr_names:
                    attr_names.append(varname)
                getters[varname] = method[1]

        for name in attr_names:
            if name not in getters:
                getters[name] = lambda self: getattr(self, name)
            if name not in setters:
                setters[name] = lambda self, value: setattr(self, name, value)

            setattr(cls, name, property(getters[name], setters[name]))
        return super().__call__(*args, **kwargs)

